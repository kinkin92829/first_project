# 測試用專案

## 在 linux 上安裝 gitlab-runner
```
# 下在啟動 runner 執行檔
sudo curl -L --output /usr/local/bin/gitlab-runner "https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64"

# 設定檔案執行權限
sudo chmod +x /usr/local/bin/gitlab-runner

# 新增執行 runner 的使用者
sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash

# 安裝成 linux system 服務
sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner

# 啟動 runner
sudo gitlab-runner start

# 註冊 runner 到 gitlab 上
export REGISTRATION_TOKEN="<專案註冊 runner 的 token"
sudo gitlab-runner register --non-interactive  --url https://gitlab.com/ --registration-token $REGISTRATION_TOKEN  --executor "shell" --description "my-shell-runner" --tag-list "ubuntu2004-runner"
```

## 在 ubuntu 安裝 nginx
```
sudo apt-get update
sudo apt-get install nginx -y

# 預設會使用 /var/www/html 目錄下的 index.html
# 要調整檔案權限

sudo chown -R gitlab-runner:gitlab-runner /var/www/html

# 停用 gitlab 上的 shared runner，頁面操作

# 若 cicd 在一開始出現錯誤有可能是初始化 bash 的設定導致
https://docs.gitlab.com/runner/faq/index.html#job-failed-system-failure-preparing-environment

```
